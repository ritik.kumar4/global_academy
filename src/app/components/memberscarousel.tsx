/* eslint-disable @next/next/no-img-element */
"use client";
import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const MembersCarousel = () => {
  const members = [
    { imageSrc: 'images/t2.jpg', name: 'Thomas Jan', location: 'Agra' },
    { imageSrc: 'images/t1.jpg', name: 'Thomas Jan', location: 'Agra' },
    { imageSrc: 'images/t3.jpg', name: 'Thomas Jan', location: 'Agra' },
    { imageSrc: 'images/t2.jpg', name: 'Thomas Jan', location: 'Agra' },
    { imageSrc: 'images/t1.jpg', name: 'Thomas Jan', location: 'Agra' },
    { imageSrc: 'images/t3.jpg', name: 'Thomas Jan', location: 'Agra' }, 
    { imageSrc: 'images/t2.jpg', name: 'Thomas Jan', location: 'Agra' },
    { imageSrc: 'images/t1.jpg', name: 'Thomas Jan', location: 'Agra' },
    { imageSrc: 'images/t3.jpg', name: 'Thomas Jan', location: 'Agra' },
  ];
  const isMobile = window.innerWidth <= 768;

  const CustomPrevButton = ({ onClick }) => (
    
    <div className="btn prev4" onClick={onClick} >
      <img src="images/l_o.png" alt="Previous" style={{
        position: 'absolute',
        top: '-40px', 
        left: isMobile ? '220px' : '186px',
        zIndex: '1', 
        
      }}
      className="btn prev4"
      onClick={onClick}/>
    </div>
  );

  const CustomNextButton = ({ onClick }) => (
    <div  style={{
      position: 'absolute',
      top: '-40px',
      right: '10px', 
    }} className="btn next4" onClick={onClick}>
      <img src="images/r_o.png" alt="Next" />
    </div>
  );

  const settings = {
    dots: false,
    infinite: true,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: <CustomPrevButton />,
    nextArrow: <CustomNextButton />,
    responsive: [
      {
        breakpoint: 768, // Adjust breakpoints as needed
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: '50px',
        },
      },
      {
        breakpoint: 480, // Adjust breakpoints as needed
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: '30px',
        },
      },
    ],
  };



  return (
    <div className="members_outer">
      <Slider {...settings}>
        {members.map((member, index) => (
          <div key={index} className="item">
            <div className="member_div">
              <img src={member.imageSrc} className="img-responsive" alt="" />
              <div className="row member_detail">
                <div className="col-xs-6">
                  <a href="#">{member.name}</a>
                </div>
                <div className="col-xs-6 text-right">
                  <p>{member.location}</p>
                </div>
              </div>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
};

export default MembersCarousel;
