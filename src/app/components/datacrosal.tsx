"use client"
import React, { useEffect, useState } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const DateCarousel = () => {
  const items = [
    { imageSrc: 'images/doc1.jpg', btnText: 'PHD', docName: 'Thomas Jan' },
    { imageSrc: 'images/doc2.jpg', btnText: 'PHD', docName: 'Thomas Jan' },
    { imageSrc: 'images/doc3.jpg', btnText: 'PHD', docName: 'Thomas Jan' },
    { imageSrc: 'images/doc1.jpg', btnText: 'PHD', docName: 'Thomas Jan' },
    { imageSrc: 'images/doc2.jpg', btnText: 'PHD', docName: 'Thomas Jan' },
    { imageSrc: 'images/doc3.jpg', btnText: 'PHD', docName: 'Thomas Jan' },
    { imageSrc: 'images/doc1.jpg', btnText: 'PHD', docName: 'Thomas Jan' },
    { imageSrc: 'images/doc2.jpg', btnText: 'PHD', docName: 'Thomas Jan' },
    { imageSrc: 'images/doc3.jpg', btnText: 'PHD', docName: 'Thomas Jan' },
    // Add more items as needed
  ];

  const [slidesToShow, setSlidesToShow] = useState(calculateSlidesToShow());


  const CustomPrevButton = ({ onClick }) => (
    <div className="customNavigation2">
      <a className="btn prev2" onClick={onClick}>
        <img src="images/l_o.png" alt="Previous" />
      </a>
    </div>
  );
  
  const CustomNextButton = ({ onClick }) => (
    <div className="customNavigation2">
      <a className="btn next2" onClick={onClick}>
        <img src="images/r_o.png" alt="Next" />
      </a>
    </div>
  );

  function calculateSlidesToShow() {
    const screenWidth = window.innerWidth;

    if (screenWidth < 768) {
      return 2;
    } else if (screenWidth < 1200) {
      return 3;
    } else {
      return items.length < 9 ? items.length : 8;
    }
  }

  useEffect(() => {
    const handleResize = () => {
      setSlidesToShow(calculateSlidesToShow());
    };

    window.addEventListener('resize', handleResize);

  
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []); 
  
  const settings = {
    dots: false,
    infinite: true,
    speed: 1000,
    slidesToShow: slidesToShow,
    slidesToScroll: 1,
    prevArrow: <CustomPrevButton />,
    nextArrow: <CustomNextButton />,
    autoplay: true,
    autoplaySpeed: 5000, 
    cssEase: "linear", 
    centerMode: true, 
   
    
  
  };
  return (
    <Slider {...settings} >
      {items.map((item, index) => (
        <div key={index} className="item" style={{ marginRight: '50px' }}>
          <div className="phd_box">
            <a href="#">
              <img src={item.imageSrc} className="center-block" alt="" />
            </a>
            <div className="clearfix" />{" "}
            <a href="#" className="btn btn_new1">
              {item.btnText}
            </a>
            <div className="clearfix" />{" "}
            <a href="#" className="doc_name">
              {item.docName}
            </a>{" "}
          </div>
        </div>
      ))}
    </Slider>
  );
};

export default DateCarousel;
