"use client"

import React from 'react';
import Slider from 'react-slick';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
const items = [
    { id: 1, imageUrl: 'tab1.jpg' },
    { id: 2, imageUrl: 'tab1b.jpg' },
    { id: 1, imageUrl: 'tab1.jpg' },
    { id: 2, imageUrl: 'tab1b.jpg' },
    { id: 1, imageUrl: 'tab1.jpg' },
    { id: 2, imageUrl: 'tab1b.jpg' },
    { id: 1, imageUrl: 'tab1.jpg' },
    { id: 2, imageUrl: 'tab1b.jpg' },
  ];

  
  const CustomPrevButton = ({ onClick }) => (
    <div className="customNavigation2">
      <a className="btn prev2" onClick={onClick}>
        <img src="images/l_o.png" alt="Previous" />
      </a>
    </div>
  );
  
  const CustomNextButton = ({ onClick }) => (
    <div className="customNavigation2">
      <a className="btn next2" onClick={onClick}>
        <img src="images/r_o.png" alt="Next" />
      </a>
    </div>
  );

const GalleryCarousel = () => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 1,
        prevArrow: <CustomPrevButton />,
        nextArrow: <CustomNextButton />,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              infinite: true,
              dots: true,
            },
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ],
      };



  return (
    <div>
      <Slider {...settings}>
        {items.map((item) => (
          <div key={item.id} className="item">
            <div className="gallery_box">
              <div className="cpl_image cpl_new">
                <a href={`images/${item.imageUrl}`} data-fancybox="images">
                  <img
                    src={`images/${item.imageUrl}`}
                    className="img-responsive center-block"
                    alt=""
                  />
                  {/* <div className="pro_plus">
                    <img
                      src="images/plus.png"
                      className="img-responsive"
                      alt=""
                    />
                  </div> */}
                </a>
              </div>
            </div>
          </div>
        ))}
      </Slider>

      {/* <div className="customNavigation3">
        <button className="btn prev3" onClick={() => this.slider.slickPrev()}>
          <img src="images/l_o.png" alt="Previous" />
        </button>
        <button className="btn next3" onClick={() => this.slider.slickNext()}>
          <img src="images/r_o.png" alt="Next" />
        </button>
      </div> */}
    </div>
  );
};

export default GalleryCarousel;
