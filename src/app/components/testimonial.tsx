"use client";
import React, { useEffect, useRef, useState } from 'react';
import Slider from 'react-slick';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const testimonialItems = [
  {
    id: 1,
    imageSrc: 'images/doc1.jpg',
    invertedImageSrc: 'images/inverted.png',
    content:
      "I'm totally unconvinced that two people can find a person they haven't known previously and become an effective co-founder.",
    name: 'Andrew Wilkins',
    title: 'Managing Director, Yess Company',
  },
  {
    id: 2,
    imageSrc: 'images/doc2.jpg',
    invertedImageSrc: 'images/inverted.png',
    content:
      "I'm totally unconvinced that two people can find a person they haven't known previously and become an effective co-founder.",
    name: 'Andrew Wilkins',
    title: 'Managing Director, Yess Company',
  },
  {
    id: 3,
    imageSrc: 'images/doc3.jpg',
    invertedImageSrc: 'images/inverted.png',
    content:
      "I'm totally unconvinced that two people can find a person they haven't known previously and become an effective co-founder.",
    name: 'Andrew Wilkins',
    title: 'Managing Director, Yess Company',
  },
];

const TESTIMONIALS = () => {
  const sliderRef = useRef(null);
  const [slidesToShow, setSlidesToShow] = useState(calculateSlidesToShow());

  useEffect(() => {
    const handleResize = () => {
      setSlidesToShow(calculateSlidesToShow());
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);


  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow:slidesToShow,
    slidesToScroll: 1,
  };

  const handlePrevClick = () => {
    sliderRef.current.slickPrev();
  };

  const handleNextClick = () => {
    sliderRef.current.slickNext();
  };

  function calculateSlidesToShow() {
    const screenWidth = window.innerWidth;

    if (screenWidth < 768) {
      return 1;
    } else if (screenWidth < 992) {
      return 2;
    } else if (screenWidth < 1200) {
      return 3;
    } else {
      return testimonialItems.length;
    }
  }

  return (
    <div>
      <Slider {...settings} ref={sliderRef}>
        {testimonialItems.map((item) => (
          <div key={item.id} className="item" style={{ margin: '0 10px' }}>
            <div className="diff_box" data-uniform="true3">
              <div className="diff_inner">
                <div className="inverted">
                  <img src={item.invertedImageSrc} alt="" />
                </div>
                <div className="clearfix" />
                <p>{item.content}</p>
                <div className="clearfix" />
                <div className="diff_left">
                  <img src={item.imageSrc} className="img-responsive" alt="" />
                </div>
                <div className="diff_right">
                  <h5>{item.name}</h5>
                  <h6>{item.title}</h6>
                </div>
                <div className="clearfix" />
              </div>
            </div>
          </div>
        ))}
      </Slider>

      <div className="customNavigation1">
        <a className="btn prev1" onClick={handlePrevClick}>
          <img src="images/l_o.png" alt="Previous" />
        </a>
        <a className="btn next1" onClick={handleNextClick}>
          <img src="images/r_o.png" alt="Next" />
        </a>
      </div>
    </div>
  );
};

export default TESTIMONIALS;
